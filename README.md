# Source Tree 範例 #

- 安裝 & 操作 Source Tree 方法依照 images 目錄內圖片檔名數字順序 (由小到大)
- 附上說明 ppt 檔案參考

----

### Git 參考說明 ###

- [30天精通Git版本控管](http://blog.miniasp.com/post/2013/11/03/Learning-Git-Part-2-Master-Git-in-30-days.aspx)

- [Git官方網站文件](https://git-scm.com/book/zh-tw/v2/%E9%96%8B%E5%A7%8B-%E9%97%9C%E6%96%BC%E7%89%88%E6%9C%AC%E6%8E%A7%E5%88%B6)
